from gi.repository import Gtk


@Gtk.Template(resource_path="/io/thomasross/mirage/fullscreen-controls.ui")
class FullscreenControls(Gtk.Widget):
    __gtype_name__ = "FullscreenControls"

    slideshow_delay_adjustment = Gtk.Template.Child()

    left_window = Gtk.Template.Child()
    slideshow_start_button = Gtk.Template.Child()
    slideshow_stop_button = Gtk.Template.Child()

    right_window = Gtk.Template.Child()

    def insert_action_group(self, *args, **kwargs):
        self.left_window.insert_action_group(*args, **kwargs)
        self.right_window.insert_action_group(*args, **kwargs)

    def set_screen(self, *args, **kwargs):
        self.left_window.set_screen(*args, **kwargs)
        self.right_window.set_screen(*args, **kwargs)

    def show_all(self, *args, **kwargs):
        self.left_window.show_all(*args, **kwargs)
        self.right_window.show_all(*args, **kwargs)

    def hide(self, *args, **kwargs):
        self.left_window.hide(*args, **kwargs)
        self.right_window.hide(*args, **kwargs)

    def modify_bg(self, *args, **kwargs):
        self.left_window.modify_bg(*args, **kwargs)
        self.right_window.modify_bg(*args, **kwargs)

    def position(self, width, height, x, y):
        left_window_height = self.left_window.get_allocation().height
        right_window_width = self.right_window.get_allocation().width
        right_window_height = self.right_window.get_allocation().height
        self.left_window.move(2 + x, int(height - left_window_height - 2))
        self.right_window.move(
            width - right_window_width - 2 + x, int(height - right_window_height - 2)
        )

    def set_slideshow_delay(self, delay):
        self.slideshow_delay_adjustment.set_value(delay)

    def set_slideshow_playing(self, playing):
        if playing:
            self.slideshow_start_button.hide()
            self.slideshow_start_button.set_no_show_all(True)
            self.slideshow_stop_button.show()
            self.slideshow_stop_button.set_no_show_all(False)
        else:
            self.slideshow_start_button.show()
            self.slideshow_start_button.set_no_show_all(False)
            self.slideshow_stop_button.hide()
            self.slideshow_stop_button.set_no_show_all(True)
